
# Changelog for Messages Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.6.1] - 2024-10-25

maven-portal-bom 4.0.0


## [v2.6.0] - 2022-09-22

 - Fix Bug #23898:  open with email addresse preset is not working anymore 
 - Moved to OpenJDK11 
 - Moved to GWT 2.9.0

## [v2.5.2] - 2022-06-16

- Released for removal HL from portal

## [v2.5.1] - 2021-09-09

- Fixed attachment issue #21972

## [v2.5.0] - 2021-07-28

- Ported to git

- Remove HomeLibrary dependency and replace with storage hub one

- Temporarely removed possibility to add attachments 

## [v2.4.0] - 2019-10-25

- Bug #17876 the message menu isn't fully displayed if there is only one message or no other messages below

- Incident #17778 direct download attachment does not work

## [v2.3.0] - 2019-02-28

- Feature #10068, Request for enhancement: new Messages is error prone and challenging to use

- Feature #16194, Isolate Gateway user list in Messages

## [v2.1.0] - 2018-06-14

- Updated Workspace Explorer dependency

- Added support for SendTo

## [v2.0.0] -2 017-09-21

- Improved look and feel

- Responsive, works on mobile

## [v1.0.0] - 2013-01-13

- First release

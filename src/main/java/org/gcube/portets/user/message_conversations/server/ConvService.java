package org.gcube.portets.user.message_conversations.server;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.gcube.portets.user.message_conversations.client.MessageService;
import org.gcube.portets.user.message_conversations.shared.ConvMessage;
import org.gcube.portets.user.message_conversations.shared.CurrUserAndPortalUsersWrapper;
import org.gcube.portets.user.message_conversations.shared.WSUser;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class ConvService extends RemoteServiceServlet implements MessageService, HttpServletRetriever {

	MessageService delegate;
	
	public void init() {
		delegate = new ConvServiceImpl(this);
	}
	
	@Override
	public ArrayList<ConvMessage> getMessages(boolean sent) {
		return delegate.getMessages(sent);
	}

	@Override
	public ConvMessage getMessageById(String messageId, boolean sent) {
		return delegate.getMessageById(messageId, sent);
	}

	@Override
	public CurrUserAndPortalUsersWrapper getWorkspaceUsers() {
		return delegate.getWorkspaceUsers();
	}

	@Override
	public ArrayList<WSUser> getUsersInfo(String[] usernames) {
		return delegate.getUsersInfo(usernames);
	}

	@Override
	public ArrayList<WSUser> searchUsers(String keyword) {
		return delegate.searchUsers(keyword);
	}

	@Override
	public boolean sendToById(ArrayList<String> recipientIds, ArrayList<String> listAttachmentsId, String subject,
			String body) {
		return delegate.sendToById(recipientIds, listAttachmentsId, subject, body);
	}

	@Override
	public boolean deleteMessageById(String messageId, boolean sent) {
		return delegate.deleteMessageById(messageId, sent);
	}

	@Override
	public String getAttachmentDownloadURL(String itemId) {
		return delegate.getAttachmentDownloadURL(itemId);
	}

	@Override
	public boolean saveAttachmentToWorkspaceFolder(String itemId, String destinationFolderId) {
		return delegate.saveAttachmentToWorkspaceFolder(itemId, destinationFolderId);
	}

	@Override
	public boolean markMessageUnread(String messageId, boolean sent) {
		return delegate.markMessageUnread(messageId, sent);
	}

	@Override
	public HttpServletRequest getRequest() {
		return getThreadLocalRequest();
	}

}

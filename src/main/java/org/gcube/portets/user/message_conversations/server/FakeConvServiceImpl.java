package org.gcube.portets.user.message_conversations.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portets.user.message_conversations.client.MessageService;
import org.gcube.portets.user.message_conversations.shared.ConvMessage;
import org.gcube.portets.user.message_conversations.shared.CurrUserAndPortalUsersWrapper;
import org.gcube.portets.user.message_conversations.shared.MessageUserModel;
import org.gcube.portets.user.message_conversations.shared.WSUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FakeConvServiceImpl implements MessageService{

	private static final Logger _log = LoggerFactory.getLogger(FakeConvServiceImpl.class);
	
	
	ArrayList<ConvMessage> receivedMessages;
	ArrayList<ConvMessage> sentMessages;
	
	Map<String,ConvMessage> receivedMessagesMap = new HashMap<>();
	Map<String,ConvMessage> sentMessagesMap = new HashMap<>();
	
	
	final int messageReceivedNumberToTest = 200;
	final int messageSentNumberToTest = 11;
	
	public FakeConvServiceImpl() {
		Calendar cal = Calendar.getInstance();
		MessageUserModel mu = new MessageUserModel("andrea.rossi");
		
		List<MessageUserModel> recipients = Arrays.asList(new MessageUserModel("lucio.lelii"));
		
		String prefix ="received";
		receivedMessages = new ArrayList<>();
		for (int i =0; i<messageReceivedNumberToTest; i++) {
			String id = "id-"+prefix+i;
			ConvMessage msg= new ConvMessage(id, prefix+i, mu, recipients, new Date(cal.getTimeInMillis()), prefix+i, false, false);
			receivedMessages.add(msg);
			receivedMessagesMap.put(id, msg);
		}
		
		prefix ="sent";
		sentMessages = new ArrayList<>();
		for (int i =0; i<messageSentNumberToTest; i++) {
			String id = "id-"+prefix+i;
			ConvMessage msg= new ConvMessage(id, prefix+i, mu, recipients,  new Date(cal.getTimeInMillis()), prefix+i, false, false);
			sentMessages.add(msg);
			sentMessagesMap.put(id, msg);
		}
		
		
		
	}
	
	
	@Override
	public ArrayList<ConvMessage> getMessages(boolean sent) {
		if (sent) {
			_log.info("sent messages call with sent {}",sent);
			return new ArrayList<>(sentMessages);
		}
		else {
			_log.info("received messages call with sent {}",sent);
			return new ArrayList<>(receivedMessages);
		}
	}

	@Override
	public ConvMessage getMessageById(String messageId, boolean sent) {
		_log.info("get messageById called with id {} and sent {}", messageId,sent );
		
		ConvMessage msg;
		if (sent)
			msg = sentMessagesMap.get(messageId);
		else msg = receivedMessagesMap.get(messageId);
		msg.setRead(true);
		return msg;
	}

	@Override
	public CurrUserAndPortalUsersWrapper getWorkspaceUsers() {
		_log.debug("trying to get WorkspaceUsers ..");
		WSUser currUser = new WSUser("andrea.rossi", "andrea.rossi", "Andrea Rossi", "m.assante@gmail.com");
		ArrayList<WSUser> portalUsers = new ArrayList<WSUser>();
		
		for (int i = 0; i < 10; i++) {
			portalUsers.add(new WSUser(""+i, "username"+i, "userGetFullname()"+i, "user.getEmail()"+i));
		}				
	
		CurrUserAndPortalUsersWrapper toReturn = new CurrUserAndPortalUsersWrapper(currUser, portalUsers);
		return toReturn;
	}

	@Override
	public ArrayList<WSUser> getUsersInfo(String[] usernames) {
		return new ArrayList<>(Arrays.asList(new WSUser("andrea.rossi", "andrea.rossi", "Andrea Rossi", "m.assante@gmail.com")));
	}

	@Override
	public ArrayList<WSUser> searchUsers(String keyword) {
		return new ArrayList<>(Arrays.asList(new WSUser("andrea.rossi", "andrea.rossi", "Andrea Rossi", "m.assante@gmail.com"))); 
	
	}

	@Override
	public boolean sendToById(ArrayList<String> recipientIds, ArrayList<String> listAttachmentsId, String subject,
			String body) {
		_log.info("email sent");
		return true;
	}

	@Override
	public boolean deleteMessageById(String messageId, boolean sent) {
		_log.info("message deleted");
		return true;
	}

	@Override
	public String getAttachmentDownloadURL(String itemId) {
		return "www.google.it";
	}

	@Override
	public boolean saveAttachmentToWorkspaceFolder(String itemId, String destinationFolderId) {
		_log.info("attachment saved");
		return true;
	}

	@Override
	public boolean markMessageUnread(String messageId, boolean sent) {
		_log.info("message marked");
		return true;
	}



}

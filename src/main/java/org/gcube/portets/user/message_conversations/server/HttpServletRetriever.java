package org.gcube.portets.user.message_conversations.server;

import javax.servlet.http.HttpServletRequest;

public interface HttpServletRetriever {

	HttpServletRequest getRequest();
}

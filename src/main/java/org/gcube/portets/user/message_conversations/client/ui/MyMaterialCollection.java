package org.gcube.portets.user.message_conversations.client.ui;

import java.util.ArrayList;
import java.util.function.Function;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent.Handler;

import gwt.material.design.client.ui.MaterialCollection;

public class MyMaterialCollection<T> extends MaterialCollection implements HasRows {

	private final int DEFAULT_LENGTH= 20;

	private boolean init_called = false;

	ArrayList<T> objects = new ArrayList<>(); 

	Function<T, Widget> converter;

	public MyMaterialCollection(ArrayList<T> objects, Function<T, Widget> function) {
		this.objects = objects; 
		this.converter = function;
	}

	int start=0;
	int length=DEFAULT_LENGTH;

	public void init(){
		if (init_called) GWT.log("init already called");
		else {
			init_called = true;
			onRangeChanged();
		}
	}

	private void onRangeChanged() {
		int widgetCountIndex = super.getWidgetCount()-1;
		int index = widgetCountIndex-1>start ? widgetCountIndex: start;
		int finish = length>objects.size()? objects.size():length ;
	
		for (int i =index; i<finish; i++) {
			super.add(converter.apply(objects.get(i)));
		}
	}
	
	public void add(Widget widget) {}

	@Override
	public HandlerRegistration addRangeChangeHandler(Handler handler) {
		return new MyHandlerRegistration();

	}

	@Override
	public HandlerRegistration addRowCountChangeHandler(
			com.google.gwt.view.client.RowCountChangeEvent.Handler handler) {
		return new MyHandlerRegistration();
	}

	@Override
	public int getRowCount() {
		GWT.log("get row count");
		return objects.size();
	}

	@Override
	public Range getVisibleRange() {
		GWT.log("get visible range");
		return new Range(start, length);
	}

	@Override
	public boolean isRowCountExact() {
		GWT.log("is row exact");
		return true;
	}

	@Override
	public void setRowCount(int count) {
		GWT.log("set row count");
	}

	@Override
	public void setRowCount(int count, boolean isExact) {
		GWT.log("set row count ex");
	}

	@Override
	public void setVisibleRange(int start, int length) {
		this.start = start;
		this.length = length;
		onRangeChanged();
	}


	@Override
	public void setVisibleRange(Range range) {
		this.setVisibleRange(range.getStart(), range.getLength());
	}

	class MyHandlerRegistration implements HandlerRegistration{
		@Override
		public void removeHandler() {
			GWT.log("handler removed");
		}

	}
}
